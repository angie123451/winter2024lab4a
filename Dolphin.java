public class Dolphin{
	
	private String species;
	private String color;
	private double jump_height;
	private boolean isEndangered; 
	
	public void introduceDolphin(){
		System.out.println ("This is a " + species + " dolphin, and it is " + color + ".");
	}
	
	public void advocateProtection(){
		if(this.isEndangered){
			System.out.println("The " + species + " dolphin is endangered, please help protect it! Humans contribute to this by overfishing, as well as water pollution and driving them out of their habitat.");
		}
		else{
			System.out.println("Thankfully, the " + species + " dolphin is not endangered, but we should still advocate to stop harming dolphins and destroying their natural habitats.");
		}
	}
	public void leap(){
		System.out.println("The dolphin leaped " + jump_height + " feet into the air!");
	}
	
	//Setter
	public void setColor(String color){
		this.color = color;
	}
	
	//Getter for all field
	public String getSpecies(){
		return this.species;
	}
	public String getColor(){
		return this.color;
	}
	public double getJump(){
		return this.jump_height;
	}
	public boolean getDanger(){
		return this.isEndangered;
	}
	
	// Contructor (default field)
	public Dolphin(String species, String color, double jump_height, boolean isEndangered){
		this.species = species;
		this.color = color;
		this.jump_height = jump_height;
		this.isEndangered = isEndangered;
	}
 	
}
