import java.util.Scanner;
public class Driver{
	
	public static void main(String args[]){
	
		Scanner reader = new Scanner(System.in);
		
		Dolphin[] pod = new Dolphin[2];
		for(int i=0; i<pod.length; i++){
			System.out.println("Enter dolphin species:");
			String species = reader.nextLine();
 			System.out.println("Enter dolphin color:");
			String color = reader.nextLine();
			System.out.println("Enter the jump height(in feet):");
			double jump_height = Double.parseDouble(reader.nextLine());
			System.out.println("True or False, Enter whether the dolphin is endangered or not:");
			boolean isEndangered = Boolean.parseBoolean(reader.nextLine());
			pod[i] = new Dolphin( species, color, jump_height, isEndangered);
		}
		
 		System.out.println("Enter new dolphin color:");
		pod[pod.length-1].setColor(reader.nextLine());
		System.out.println(pod[pod.length-1].getSpecies()+"|"+pod[pod.length-1].getColor()+"|"+pod[pod.length-1].getJump()+"|"+pod[pod.length-1].getDanger());
		
	}
}
